package dvd.servlet;

import java.io.IOException;
import java.io.PrintWriter;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import dvd.model.Dvd;
import dvd.queue.Emit;


public class DvdServlet extends HttpServlet {

	private static final long serialVersionUID = 1L;

	public void init() throws ServletException {
		
	}

	public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

		response.setContentType("text/html");
		PrintWriter out = response.getWriter();

		out.println("<form method=\"post\">  \r\n"
				+ "Title:<input type=\"text\" name=\"title\"/><br/><br/>  \r\n"
				+ "Year:<input type=\"text\" name=\"year\"/><br/><br/>  \r\n"
				+ "Price:<input type=\"text\" name=\"price\"/><br/><br/>  \r\n"
				+ "<input type=\"submit\" value=\"send\"/>  \r\n" + "</form>  ");
		System.out.println("AM PRIMIT DVD"+out.toString());
		out.close();
	}
	
	public void doPost(HttpServletRequest request, HttpServletResponse response)  
	        throws ServletException, IOException {  
	  
	    response.setContentType("text/html");  
	    String n=request.getParameter("title");  
	    String p=request.getParameter("year");  
	    String z=request.getParameter("price");
	    
	    Dvd dvd=new Dvd(n,Integer.parseInt(p),Double.parseDouble(z));
	    
	    Emit e=new Emit();
	    try {
	    	System.out.println("YUPII AM TRIMIS DVD"+dvd.toString());
			e.emit(dvd.toString());
		} catch (Exception e1) {
			System.out.println("CEVA 3EXCEPTIE");
			e1.printStackTrace();
		}
	    
	}
	
	public void destroy() {
		// do nothing.
	}

}
