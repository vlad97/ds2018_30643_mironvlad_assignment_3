package dvd.model;

public class Dvd {
	
	private String title;
	private int year;
	private double price;

	
	public Dvd(String title, int year, double price) {
		super();
		this.title = title;
		this.year = year;
		this.price = price;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public int getYear() {
		return year;
	}
	public void setYear(int year) {
		this.year = year;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	
	@Override
	public String toString() {
		// TODO Auto-generated method stub
		return this.title + " " + this.year + " " + this.price;
	}
 
 
 
}
