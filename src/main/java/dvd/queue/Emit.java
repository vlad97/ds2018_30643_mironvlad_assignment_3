package dvd.queue;

import com.rabbitmq.client.BuiltinExchangeType;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

public class Emit {

	private static final String EXCHANGE_NAME = "dvds";

	public void emit(String message) throws Exception {
		ConnectionFactory factory = new ConnectionFactory();
		factory.setHost("localhost");
		Connection connection = factory.newConnection();
		Channel channel = connection.createChannel();

		channel.exchangeDeclare(EXCHANGE_NAME, BuiltinExchangeType.FANOUT);

		channel.basicPublish(EXCHANGE_NAME, "", null, message.getBytes("UTF-8"));

		channel.close();
		connection.close();
	}
}
